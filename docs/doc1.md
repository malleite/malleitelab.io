---
id: doc1
title: Latin-ish
sidebar_label: Example Page
---

<link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/katex@0.11.0/dist/katex.min.css"
  integrity="sha384-BdGj8xC2eZkQaxoQ8nSLefg4AV4/AwB3Fj+8SUSo7pnKP6Eoy18liIKTPn9oBYNG"
  crossOrigin="anonymous"
/>


Check the [documentation](https://docusaurus.io) for how to use Docusaurus.

## Lorem
A Vaca foi pro brejo e levou o **boi** junto ..... :monkey: :monkey:


```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```


```math
a^2+b^2=c^2
```


>>>
Tem cafe hoje ?
Claro que sim !!!
>>>


```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```



Mass slice (GeV)	W->e+nu			W->e-nu			W->mu+nu			W->mu-nu			
	MC16a	MC16d	MC16e	MC16a	MC16d	MC16e	MC16a	MC16d	MC16e	MC16a	MC16d	MC16e
120-180	4.8E+06	6E+06	8E+06	4.8E+06	6E+06	8E+06	4.8E+06	6E+06	8E+06	4.8E+06	6E+06	8E+06
180-250	1.6E+06	2E+06	2.7E+06	1.6E+06	2E+06	2.7E+06	1.6E+06	2E+06	2.7E+06	1.6E+06	2E+06	2.7E+06
250-400	8E+05	1E+06	1.3E+06	8E+05	1E+06	1.3E+06	8E+05	1E+06	1.3E+06	8E+05	1E+06	1.3E+06
400-600	1.6E+05	2E+05	2.7E+05	1.6E+05	2E+05	2.7E+05	1.6E+05	2E+05	2.7E+05	1.6E+05	2E+05	2.7E+05
600-800	8E+04	1E+05	1.3E+05	8E+04	1E+05	1.3E+05	8E+04	1E+05	1.3E+05	8E+04	1E+05	1.3E+05
800-1000	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
1000-1250	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
1250-1500	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
1500-1750	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
1750-2000	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
2000-2250	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
2250-2500	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
2500-2750	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
2750-3000	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
3000-3500	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
3500-4000	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
4000-4500	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
4500-5000	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
5000	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04	5E+04	5E+04	9E+04
												
sub-Total Events 	8.14E+06	1E+07	1.366E+07	8.14E+06	1E+07	1.366E+07	8.14E+06	1E+07	1.366E+07	8.14E+06	1E+07	1.366E+07
												
Total Events	1.272E+08											
												








Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

## Mauris In Code

```
Mauris vestibulum ullamcorper nibh, ut semper purus pulvinar ut. Donec volutpat orci sit amet mauris malesuada, non pulvinar augue aliquam. Vestibulum ultricies at urna ut suscipit. Morbi iaculis, erat at imperdiet semper, ipsum nulla sodales erat, eget tincidunt justo dui quis justo. Pellentesque dictum bibendum diam at aliquet. Sed pulvinar, dolor quis finibus ornare, eros odio facilisis erat, eu rhoncus nunc dui sed ex. Nunc gravida dui massa, sed ornare arcu tincidunt sit amet. Maecenas efficitur sapien neque, a laoreet libero feugiat ut.
```

## Nulla

Nulla facilisi. Maecenas sodales nec purus eget posuere. Sed sapien quam, pretium a risus in, porttitor dapibus erat. Sed sit amet fringilla ipsum, eget iaculis augue. Integer sollicitudin tortor quis ultricies aliquam. Suspendisse fringilla nunc in tellus cursus, at placerat tellus scelerisque. Sed tempus elit a sollicitudin rhoncus. Nulla facilisi. Morbi nec dolor dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras et aliquet lectus. Pellentesque sit amet eros nisi. Quisque ac sapien in sapien congue accumsan. Nullam in posuere ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin lacinia leo a nibh fringilla pharetra.

## Orci

Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin venenatis lectus dui, vel ultrices ante bibendum hendrerit. Aenean egestas feugiat dui id hendrerit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur in tellus laoreet, eleifend nunc id, viverra leo. Proin vulputate non dolor vel vulputate. Curabitur pretium lobortis felis, sit amet finibus lorem suscipit ut. Sed non mollis risus. Duis sagittis, mi in euismod tincidunt, nunc mauris vestibulum urna, at euismod est elit quis erat. Phasellus accumsan vitae neque eu placerat. In elementum arcu nec tellus imperdiet, eget maximus nulla sodales. Curabitur eu sapien eget nisl sodales fermentum.

## Phasellus

Phasellus pulvinar ex id commodo imperdiet. Praesent odio nibh, sollicitudin sit amet faucibus id, placerat at metus. Donec vitae eros vitae tortor hendrerit finibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque vitae purus dolor. Duis suscipit ac nulla et finibus. Phasellus ac sem sed dui dictum gravida. Phasellus eleifend vestibulum facilisis. Integer pharetra nec enim vitae mattis. Duis auctor, lectus quis condimentum bibendum, nunc dolor aliquam massa, id bibendum orci velit quis magna. Ut volutpat nulla nunc, sed interdum magna condimentum non. Sed urna metus, scelerisque vitae consectetur a, feugiat quis magna. Donec dignissim ornare nisl, eget tempor risus malesuada quis.
